#pragma once
#include<iostream>
/**
 * @file Encryption.h
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This class includes functions that encrypting and decrypting the incoming codes and writing them in a format
 *
 */
class Encryption
{
public:
    /** Used to encrypting the incoming codes
    * @return encrypted code
    */
	int encrypt(int);
    /** Used to decrypting the incoming codes
    * @return decrypted code
    */
	int decrypt(int);
    /** Used to write zero left of code if the code is like 0189 
   */
	void writecode(int);
};

