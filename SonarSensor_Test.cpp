#include "PioneerRobotAPI.h"
#include "SonarSensor.h"
//#include "Aria/Aria.h"
#include <iostream>
using namespace std;
int main() {
	SonarSensor* sensor = new SonarSensor;
	PioneerRobotAPI* robot = new PioneerRobotAPI;
	float ranges[16];

	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}
	robot->moveRobot(100);

	cout << endl << "SonarSensor::updateSensor Test" << endl;
	robot->getSonarRange(ranges);
	sensor->updateSensor(ranges);
	ranges == NULL ? cout << "SonarSensor::updateSensor Test is failed!" : cout << "SonarSensor::updateSensor Test is successful!" << endl;

	cout << endl << "SonarSensor::getRange Test" << endl;
	cout << "Ranges of Sonar Sensor are [ ";
	for (int i = 0; i < 16; i++) {
		cout << sensor->getRange(i) << " ";
	}
	cout << "]" << endl;

	cout << endl << "SonarSensor::operator[] Test" << endl;
	cout << "Ranges of Sonar Sensor are [ ";
	for (int i = 0; i < 16; i++) {
		cout << sensor->operator[](i) << " ";
	}
	cout << "]" << endl;

	int index = 0;
	cout << endl << "SonarSensor::getMin Test" << endl;
	cout << "Minimum range is " << sensor->getMin(index) << " [index: " << index << "]" << endl;

	cout << endl << "SonarSensor::getMax Test" << endl;
	cout << "Maximum range is " << sensor->getMax(index) << " [index: " << index << "]" << endl;

	cout << endl << "SonarSensor::getAngle Test" << endl;
	cout << "Angles of Sonar Sensor are [ ";
	for (int i = 0; i < 16; i++) {
		cout << sensor->getAngle(i) << " ";
	}
	cout << "]" << endl;


	cout << endl << endl;
	system("pause");
}

