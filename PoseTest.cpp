#include "Pose.h"
#include <stdio.h>
int main()
{
	Pose pos1, pos2(3, 4, 70), pos3(9, 8, 40);

	printf("-----------SET TEST-----------\n");
	printf("pos1: X: %.2f  Y: %.2f  Angle: %.2f\n", pos1.getX(), pos1.getY(), pos1.getTh());
	printf("pos2: X: %.2f  Y: %.2f  Angle: %.2f\n", pos2.getX(), pos2.getY(), pos2.getTh());
	printf("pos3: X: %.2f  Y: %.2f  Angle: %.2f\n", pos3.getX(), pos3.getY(), pos3.getTh());
	printf("------------------------------\n");

	if (pos2 < pos3) printf("pos2 is less than pos3. \n");
	if (pos3 < pos2) printf("pos3 is less than pos2. \n");
	
	Pose pos4 = pos2 + pos3;
	printf("pos2 + pos3 = X: %.2f  Y: %.2f  Angle: %.2f\n", pos4.getX(), pos4.getY(), pos4.getTh());

	Pose pos5 = pos4;
	Pose pos6 = pos5 - pos4;
	printf("X: %.2f  Y: %.2f  Angle: %.2f\n", pos6.getX(), pos6.getY(), pos6.getTh());
	if (pos1 == pos6) printf("pos1 equals pos6\n");
	
	pos2 += pos3;
	printf("pos2+= pos3 = X: %.2f  Y: %.2f  Angle: %.2f\n", pos2.getX(), pos2.getY(), pos2.getTh());

	pos3 -= pos2;
	printf("pos3 -= pos2 = X: %.2f  Y: %.2f  Angle: %.2f\n", pos3.getX(), pos3.getY(), pos3.getTh());

	pos1.setPose(6, 8, 45);
	printf("pos1 : X: %.2f  Y: %.2f  Angle: %.2f\n", pos1.getX(), pos1.getY(), pos1.getTh());
	
	printf("Angle between pos1 and pos3: %.2f\n", pos1.findAngleTo(pos3));

	printf("Distance between pos1 and pos3: %.2f\n", pos1.findDistanceTo(pos3));
	printf("Distance between pos1 and pos3: %.2f\n", pos3.findDistanceTo(pos1));

	float x, y, th;
	pos1.getPose(&x, &y, &th);
	printf("pos1 : X: %.2f  Y: %.2f  Angle: %.2f\n", pos1.getX(), pos1.getY(), pos1.getTh());
	printf("pos1 : X: %.2f  Y: %.2f  Angle: %.2f\n", x, y, th);
	return 0;
}