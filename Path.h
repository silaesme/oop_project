/**
* @file Path.h
* @Author Sıla EŞME (152120181004@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief This file includes Path class's definiton.
*/
#ifndef _PATH_H_
#define _PATH_H_

#include "Pose.h"
#include "Node.h"

/** @class Path Path.h "Path.h"
 *  @brief This is a Path class.
 * This class provides manage robot's route by using Linked list. Positions that type-Pose can be added to list.
 */
class Path
{
private:

	Node* head; /**< This is a head pointer of the list. */
	Node* tail; /**< This is a tail pointer of the list. */
	int number; /**< This is a integer variable that holds how many nodes in the list.*/
public:
	/** Default constructor.
	*/
	Path() { head = NULL; tail = NULL; number = 0; }
	/** Adds given data to end of the list.
	* @param Pose pose 
	* It is added to the end of the list.
	*/
	void addPos(Pose pose);
	/** Removes given data to the given index.
	* @param int index
	*/
	void removePos(int index);
	/** Adds given data to the given index.
	* @param int index
	* : index
	* @param Pose pose
	* : Data
	*/
	void insertPos(int index, Pose pose);
	/** This function is for getting data to the given index.
	* @param i
	* : is index
	* @return Pose
	*/
	Pose getPos(int i)const;
	/** Prints positions that list includes in a proper format.
	*/
	void print()const;
	/** This operator provides access members of the linked list like an array to users.
	* @param i
	* : index
	* @return Pose
	*/
	Pose operator[](int i)const;
};

#endif // !_PATH_H_