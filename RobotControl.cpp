#include "RobotControl.h"
#include"PioneerRobotAPI.h"
#include "Path.h"
#include "Record.h"
#include "PioneerRobotInterface.h"
/**
 * @file RobotControl.cpp
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This class includes functions that enable the robot to move and direct.
 *
 */
float sonars[16];
float laserData[1000];
RobotControl::RobotControl(RobotInterface* robotI, RangeSensor* range, RobotOperator* rope) {
	position = new Pose();
	path = new Path();
	accesControl = 1;
	Roperator = rope;
	Roperator->print();
	robot = robotI;
	laser = range;
	//robot->updateSensors(*laserData);
	robot->setPose(*position);
	accesControl = 1;
}
RobotControl::~RobotControl() {
	delete position;
	delete robot;
}
bool RobotControl::connect() {
	return robot->connect();
}
bool RobotControl::disconnect() {
	return robot->disconnect();
}
void RobotControl::turnLeft() {
	if (accesControl == 1)
		robot->turnLeft();
}
void RobotControl::turnRight() {
	if (accesControl == 1)
		robot->turnRight();
}
/**
*Multiplied the speed by (-) to get the robot back
*/
void RobotControl::forward(float speed) {
	if (accesControl == 1) {
		if (speed < 0)
			speed *= -1;
		robot->forward(speed);
	}
}
void RobotControl::print() {
	if (accesControl == 1) {
		*position = robot->getPose(*position);
		cout << "MyPose is (" << position->getX() << "," << position->getY() << "," << position->getTh() << ")" << endl;
		robot->getSonar(*sonars);
		cout << "Sonar ranges are [ ";
		for (int i = 0; i < 16; i++) {
			cout << sonars[i] << "\t ";
		}
		cout << "]" << endl;
		robot->getLaser(*laserData);
		cout << "Laser ranges are [ ";
		for (int i = 0; i < 181; i++) {
			cout << laserData[i] << "\t";
		}
		cout << "]" << endl;
	}
	else {
		cout << "Acces code is False!!!!!!! No access!" << endl;
	}
}
void RobotControl::backward(float speed) {
	if (accesControl == 1)
		robot->forward(speed);
}
Pose RobotControl::getPose() {
	Pose temp;
	if (accesControl == 1) {
		Pose temp; ///Defined a temporary Pose object to rotate the Pose object.
		robot->getPose(temp);
		position = &temp;
	}
	return temp;
}
/**
*Set the class pose object attributes with the incoming location information
*/
void RobotControl::setPose(Pose& _position) {
	if (accesControl == 1) {
		position = &_position;
		robot->setPose(_position);
	}
}
void RobotControl::stopTurn() {
	if (accesControl == 1)
		robot->stopTurn();
}
void RobotControl::stopMove() {
	if (accesControl == 1)
		robot->stopMove();
}
bool RobotControl::addToPath() {
	if (accesControl == 1) {
		path->addPos(*position);
		return true;
	}
	return false;
}
bool RobotControl::clearPath() {
	if (accesControl == 1) {
		int index = path->getNumber();
		while (index > 0) { //Deletes all positions in the path
			path->removePos(0);
			index = path->getNumber();
			cout << endl;
		}
		return true;
	}
	return false;
}
bool RobotControl::recordPathToFile() {
	if (accesControl == 1) {
		Pose _poisition;
		Record record;
		float x, y, th;
		string sst, sy, st;
		record.setFileName("path.txt"); //names the file to be opened.
		record.openFile();
		int index = path->getNumber();
		for (int i = 0; i < index; i++) {
			_poisition = path->getPos(i); //Retrieves registered positions in path
			x = _poisition.getX();
			y = _poisition.getY();
			th = _poisition.getTh();
			sst = to_string(x);
			sy = to_string(y);
			st = to_string(th);
			record.writeLine("X = " + sst + "Y = " + sy + "Angle = " + st); //print position information to file
		}
		record.closeFile();
		return true;
	}
	return false;
}
bool RobotControl::openAccess(int code) {
	if (Roperator->checkAccessCode(code)) {
		cout << "Access code is TRUE!" << endl;
		accesControl = 1;
		return true;
	}
	else
		accesControl = 0;
	cout << "Access code is FALSE!" << endl;
	return false;
}
bool RobotControl::closeAccess(int code) {
	if (Roperator->checkAccessCode(code)) {
		cout << "Access Code is TRUE! " << endl;
		cout << "Access closed successfully!" << endl;
		accesControl = 0;
		return true;
	}
	cout << "Access Code is FALSE! " << endl;
	cout << "Access not closed!" << endl;
	return false;
}