/**
* @file PioneerRobotInerface.cpp
* @Author Sila ESME (152120181004@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief This file includes Pose PioneerRobotInerface's declaration.
*/
#include "PioneerRobotInterface.h"
PioneerRobotInterface::PioneerRobotInterface(RangeSensor* range)
{
	//robotAPI = new PioneerRobotAPI;
	laser = range;
}

void PioneerRobotInterface::turnLeft()
{
	robot->turnRobot(PioneerRobotAPI::DIRECTION::left);
}

void PioneerRobotInterface::turnRight()
{
	robot->turnRobot(PioneerRobotAPI::DIRECTION::right);
}

void PioneerRobotInterface::forward(float speed)
{
	robot->moveRobot(speed);
}

void PioneerRobotInterface::backward(float speed)
{
	speed = -speed;
	robot->moveRobot(speed);
}

Pose PioneerRobotInterface::getPose(Pose& pose)
{
	float x, y, th;
	x = robot->getX();
	y = robot->getY();
	th = robot->getTh();
	position->setPose(x, y, th);
	pose = *position;
	return pose;
}

void PioneerRobotInterface::setPose(Pose pose)
{
	position->setPose(pose.getX(), pose.getY(), pose.getTh());
}

void PioneerRobotInterface::stopTurn()
{
	robot->stopRobot();
}

void PioneerRobotInterface::stopMove()
{
	robot->stopRobot();
}

void PioneerRobotInterface::updateSensors()
{
	float x, y, th;
	float lasers[181];
	x = robot->getX();
	y = robot->getY();
	th = robot->getTh();
	position->setX(x);
	position->setY(y);
	position->setTh(th);
	for (int i = 0; i < 181; i++)
		lasers[i] = laser->getRange(i);
	laser->updateSensor(lasers);
}
void PioneerRobotInterface::getLaser(float& lasers) {
	robot->getLaserRange(&lasers);
}
void PioneerRobotInterface::getSonar(float& sonars) {
	robot->getSonarRange(&sonars);
}
