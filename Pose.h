/**
* @file Pose.h
* @Author Sıla EŞME (152120181004@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief This file includes Pose class's definiton.
*/
#ifndef _POSE_H_
#define _POSE_H_

#include "PioneerRobotAPI.h"

/** This class has the task of holding and managing the robot's information. 
* "th" indicates robot's orientation.
* x and y take values ​​in millimeters. th takes value in degree units.
*/
class Pose
{
private:
	float x, y, th;
public:
	/** Default constructor.
	* It sets x, y, th to zero.
	*/
	Pose();
	/** Parametered constructor.
	* It sets x, y, th values with using setPose function.
	*/
	Pose(float _x, float _y ,float _th);
	~Pose();
	/** Sets x value. Using by setPose function.
	* @param is given x value.
	*/
	void setX(float _x);
	/** Sets th value. Using by setPose function.
	* @param is given th value.
	*/
	void setTh(float _th);
	/** Sets y value. Using by setPose function.
	* @param is given y value.
	*/
	void setY(float _y);
	/** 
	* @return X value.
	*/
	float getX();
	/**
	* @return th value.
	*/
	float getTh();
	/**
	* @return y value.
	*/
	float getY();
	/** Compares two Pose's values.
	* @return true if all Pose values are equal.
	*/
	bool operator==(const Pose& other);
	/** Adds two Pose's values.
	* @param right Pose.
	* @return summation value.
	*/
	Pose operator+(const Pose& other);
	/** Extracts two Pose's values.
	* @param right Pose.
	* @return extraction value.
	*/
	Pose operator-(const Pose& other);
	/** Adds right value to the left value.
	* @param right Pose.
	* @return brand new left Pose.
	*/
	Pose& operator+=(const Pose& other);
	/** Extracts right value to the left value.
	* @param right Pose.
	* @return brand new left Pose.
	*/
	Pose& operator-=(const Pose& other);
	/** Compares two Pose's values. Checks if left Pose is less than right Pose.
	* @return true if left Pose is less than the right Pose.
	*/
	bool operator<(const Pose& other);
	/** Prints position (Pose) variables x, y, th.
	* @param fof is output variable.
	* @param pose is type Pose position variable.
	* @return output variable.
	*/
	friend std::ostream& operator<<(std::ostream& fof, Pose& pose);
	/** Takes position (Pose) variables x, y, th.
	* @param fin is input variable.
	* @param pose is type Pose position variable.
	* @return input variable.
	*/
	friend std::istream& operator>>(std::istream& fin, Pose& pose);
	/** Returns x, y, th values which position (Pose) private variables.
	* @param *_x takes Pose::x value.
	* @param *_y takes Pose::y value.
	* @param *_th takes Pose::th value.
	* @return three values of them when parameter is reference of variable. Example: getPose(&x,&y,&th)
	*/
	void getPose(float* _x, float* _y, float* _th);
	/** Set function to position.
	* Sets all values which x ,y ,th.
	* @param _x to set value x.
	* @param _y to set value y.
	* @param _th to set value th.
	*/
	void setPose(float _x, float _y, float _th);
	/** It finds distance between two 2D points. 
	* @param pos is second position.
	* @return distance value.
	*/
	float findDistanceTo(Pose pos);
	/** It finds angle between two 2D vectors.
	* @param pos is second position.
	* @return radian angle value.
	*/
	float findAngleTo(Pose pos);
};
#endif // !_POSE_H_

