#include <iostream>
#include "RobotControl.h"
#include "PioneerRobotAPI.h"
#include "PioneerRobotInterface.h"
using namespace std;
/**
 * @file RobotControlTest.cpp
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This cpp tests the operation of the RobotControl class
 *
 */
RobotControl* control;
int main() {
	/*!
	 \param temp a Pose object.
	 \param control a RobotControl object.
	*/
	Pose* temp = new Pose(0.0, 0.0, 0.0);
	RobotOperator* Roperator = new RobotOperator("Merve", "Kati", 1, 1234);
	LaserSensor* laser = new LaserSensor;
	PioneerRobotInterface* robot = new PioneerRobotInterface(laser);
	control = new RobotControl(robot, laser, Roperator);
	int acces;
	///If it can't connect to the robot, it can't command the robot
	if (!control->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}
	cout << "Please Enter acces Code : ";
	cin >> acces;
	control->openAccess(acces);
	control->forward(5000);
	control->addToPath();
	control->print();
	Sleep(1000);


	control->turnLeft();
	Sleep(1000);
	control->addToPath();
	control->print();

	control->turnRight();
	Sleep(1000);
	control->addToPath();
	control->print();
	control->stopTurn();

	cout << "Please Enter acces Code to stop everything: ";
	cin >> acces;
	control->closeAccess(acces);
	control->stopTurn();
	control->addToPath();
	Sleep(1000);

	control->backward(1000);
	*temp = control->getPose();
	control->setPose(*temp);
	Sleep(1000);
	control->addToPath();
	control->print();

	control->forward(5000);
	control->print();
	control->addToPath();
	Sleep(1000);

	control->stopMove();
	cout << endl << "path.txt is opened and printed. " << endl;
	control->recordPathToFile();
	control->clearPath();
	cout << "Press any key to exit...";
	getchar();
	///After the commands are finished, it is necessary to disconnect from the robot
	control->disconnect();
	delete control;
	return 0;

}