#include "Encryption.h"
#include <iostream>
using namespace std;
/**
 * @file EncryptionTest.cpp
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This cpp used to test Encryption class
 *
 */
int main() {
	Encryption *crypt;
	crypt = new Encryption();
	int code,encr;
	///The user is prompted to enter the number user wants to be encrypted.
	cout << "Enter a 4 digit number : ";
	cin >> code;
	encr = crypt->encrypt(code);
	if (encr != -1) {
		cout << "Encrypted version of the entered number : ";
		crypt->writecode(encr);
		cout<<endl;
	}
	///The user is prompted to enter the encrypted code user wants to be decrypted.
	cout << "Enter a 4 digit encrypted number : " ;
	cin >> code;
	encr = crypt->decrypt(code);
	if (encr != -1) {
		cout << "Decrypted version of the entered number : ";
		crypt->writecode(encr);
			cout<<endl;
	}
	system("pause");
}