#include "Path.h"
#include <stdio.h>
#include <iostream>
void Test1();
int main() 
{
	Test1();
	return 0;
}

void Test1()
{
	Path LinkedTest;
	Pose pos1(4, 8, 45), pos2(6, 7, 60), pos3(2, 7, 25), pos4(12, 9, 270), pos5(9, 12, 125), pos6(4, 3, 45);

	printf("Adding pos1 = 4, 8, 45...\n"); LinkedTest.addPos(pos1); // pos1
	LinkedTest.print();
	printf("Adding pos2 = 6, 7, 60...\n"); LinkedTest.addPos(pos2); // pos1 --> pos2
	LinkedTest.print();
	printf("Adding pos3 = 2, 7, 25...\n"); LinkedTest.addPos(pos3); // pos1 --> pos2 --> pos3
	LinkedTest.print();
	printf("Inserting pos4 to index 1...\n"); LinkedTest.insertPos(1, pos4); // pos1 --> pos2 --> pos4 --> pos3
	LinkedTest.print();
	printf("Inserting pos5 to index 3...\n"); LinkedTest.insertPos(3, pos5); // pos1 --> pos2 --> pos4 --> pos3 --> pos5
	LinkedTest.print();
	printf("Inserting pos6 to index 5...\n"); LinkedTest.insertPos(5, pos6); // pos1 --> pos2 --> pos4 --> pos3 --> pos5 -- INVALID INDEX
	LinkedTest.print();
	printf("Deleting pos1 to index 0...\n"); LinkedTest.removePos(0); // pos2 --> pos4 --> pos3 --> pos5
	LinkedTest.print();
	printf("Deleting pos3 to index 2...\n"); LinkedTest.removePos(2); // pos2 --> pos4 --> pos5
	LinkedTest.print();
	printf("Deleting pos5 to index 2...\n"); LinkedTest.removePos(2); // pos2 --> pos4
	LinkedTest.print();

	Pose pos7 = LinkedTest[1]; // pos7 = pos4
	std::cin >> pos7; std::cout << pos7;
}