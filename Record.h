#pragma once
#include<fstream>
#include<iostream>
#include<string>
/**
* @file Record.h
* @Author Sanem Y�ld�z Kavuko�lu (152120181043@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief This file includes Record class's definiton.
*/
using namespace std;
class Record
{
/*! \class Record Record.h
 *  \brief Record class
 *
 *  This class runs the file operations.
 *
 */
private:
	/*! \brief fstream object
	*
	*	"file" controls the related text file.
	*
	*/
	fstream file;
	/*! \brief string variable
	*
	*	"fileName" keeps the name of the file.
	*
	*/
	string fileName;
public:
	/*! \brief openFile function
	*
	*	creates a file to read and write.
	* @return true the file is open.
	*
	*/
	bool openFile();	
	/*! \brief closeFile function
	*
	*	closes the open file.
	* @return true the file is closed.
	*
	*/
	bool closeFile();	
	/*! \brief setFileName function
	*
	*	sets the name of the file.
	*
	*/
	void setFileName(string);		
	/*! \brief readLine function
	*
	*	reads a line of information from the file.
	* @return the line that has been read.
	*
	*/
	string readLine();		
	/*! \brief writeLine function
	* 
	* @param a line of string.
	*	writes a line of information to the file.
	* @return true if process is done.
	*
	*/
	bool writeLine(string);	
	/*! \brief OpenExistingFile function
	*
	*	opens an existing file to read or write .
	* @return true the file is open.
	*
	*/
	bool OpenExistingFile();
	
};