#include "RobotOperator.h"
/**
 * @file RobotOperator.cpp
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This class includes functions that gets Operator information
 *
 *This class provide the robot operator through composition relationship between
 * classes Encryption and RobotOperator
 */
RobotOperator::RobotOperator():accessCode(1111){ //accessCode is const 
	name = "NULL";
	surname = "NULL";
	accessState = 0;
	controlcode = 1111;
}
RobotOperator::RobotOperator(string _name, string _surname, bool acc,int code):accessCode(code){
	name = _name;
	surname = _surname;
	accessState = acc;
}
RobotOperator ::~RobotOperator() {
}
string RobotOperator::getName() {
	return name;
}
string RobotOperator::getSurName() {
	return surname;
}
int RobotOperator::getAccessCode() {
	return accessCode;
}
void RobotOperator::setName(string _name) {
	name = _name;
}
void RobotOperator::setSurName(string _sur) {
	surname = _sur;
}
void RobotOperator::setaccessState(bool state) {
	accessState = state;
}
int RobotOperator::encryptCode(int code) {
	controlcode = enc.encrypt(code);
	return controlcode;
}
int RobotOperator::decryptCode(int code) {
	int opencode;
	opencode = enc.decrypt(code);  //use Encryption class function
	return opencode;
}
bool RobotOperator::checkAccessCode(int code) {
	if (accessCode == code)
		return true;

	return false;
}
bool RobotOperator::checkAccessState() {
	if (accessState>= 1)
		return true;
	return false;
}
void RobotOperator::print() {
	cout << "The operator" << endl << "Name : " << name << endl<< "Surname : " << surname<<endl;
	cout << "Access state : ";
	if (checkAccessState())
		cout << "There is access" << endl;
	else
		cout << "There isn't access" << endl;
	cout << "The Access code : " <<accessCode<<endl;
}