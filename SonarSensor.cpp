#include "SonarSensor.h"
#include "PioneerRobotAPI.h"
SonarSensor::SonarSensor() {
	robotAPI = new PioneerRobotAPI;
	robotAPI->getSonarRange(ranges);
}

SonarSensor::~SonarSensor() {
	delete robotAPI;
}

float SonarSensor::getRange(int index) { return ranges[index]; }

float SonarSensor::getMax(int& index) { 
	float max = ranges[0];
	index = 0;
	for (int i = 1; i < 16; i++) {
		if (ranges[i] > max) {
			max = ranges[i];
			index = i;
		}	
	}
	return max; 
}

float SonarSensor::getMin(int& index) { 
	float min = ranges[0];
	index = 0;
	for (int i = 1; i < 16; i++) {
		if (ranges[i] < min) {
			min = ranges[i];
			index = i;
		}
	}
	return min;
}

void SonarSensor::updateSensor(float ranges[]) {
	for (int i = 0; i < 16; i++)
		this->ranges[i] = ranges[i];
}

float SonarSensor::operator[](int i) { return ranges[i]; }

float SonarSensor::getAngle(int index) { return angle[index]; }

float SonarSensor::getClosestRange(float startAngle, float endAngle, float& angle) {
	
	float min_distance = ranges[(int)startAngle];
	angle = startAngle;
	for (int i = int(startAngle); i <= int(endAngle); i++) {
		if (min_distance > ranges[i]) {
			min_distance = ranges[i];
			angle = this->angle[i];
		}
	}
	return min_distance;
}