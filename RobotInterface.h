#pragma once
#include "Pose.h"
#include "PioneerRobotAPI.h"
#include "RobotControl.h"
#include<iostream>
using namespace std;
/**
 * @file RobotInterface.h
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This class abstract class.
 *
 * This class provide the robot move on, to go back and turn through inheritance with PioneerRobotInterface.
 *RobotInterface is an abstract class, with PioneerRobotAPI
 *It creates an interface to the associated PineerRobotInterface. So a different robot
 *When an API belonging to the same robot or a different API of the same robot is used, the RobotInterface class
 *another class that inherits will be added. The rest of our software will not be affected by this addition.
 */
class RobotControl;
class RobotInterface {
protected:
	Pose* position;
	int state;
	PioneerRobotAPI* robot;
public:
	RobotInterface() {
		position = new Pose();
		state = 1;
		robot = new PioneerRobotAPI();
	}
	/** Used to connect to the robot. Ths function should be called before others.
	* @return true if the connection is success, otherwise false
	*/
	bool connect() {
		robot->connect();
		return true;
	}
	/** Used to disconnect to the robot. Ths function should be called before quiting from program.
	* @return true if the connection is success, otherwise false
	*/
	bool disconnect() {
		robot->disconnect();
		return true;
	}
	virtual void turnLeft() = 0;
	virtual void turnRight() = 0;
	virtual void forward(float) = 0;
	void print() {
	}
	virtual void backward(float) = 0;
	virtual Pose getPose(Pose&) = 0;
	virtual void setPose(Pose) = 0;
	virtual void stopTurn() = 0;
	virtual void stopMove() = 0;
	virtual void updateSensors() = 0;
	virtual void getLaser(float&) = 0;
	virtual void getSonar(float&) = 0;
};
