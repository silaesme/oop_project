#pragma once
#include"PioneerRobotAPI.h"
#include"PioneerRobotInterface.h"
#include"RobotControl.h"
#include "Aria/Aria.h"
#include "LaserSensor.h"
#include"SonarSensor.h"
#include "Pose.h"
/**
* @file Menu.h
* @Author Sanem Y�ld�z Kavuko�lu (152120181043@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief This file includes Menu class's definiton.
*/
class Menu
{
/*! \class Menu Menu.h
 *  \brief This is a Menu class
 *
 *  This class allows user control the related robot by selection in declared options.
 *
 */
private:
	/*! \brief This is a SonarSensor class object
	*
	*	"ss" allows the user to reach to the Sonar sensor values.
	*
	*/
	SonarSensor* ss;
	/*! \brief This is a RobotOperator class object
	*
	*	"RobotOp" allows the user to use the operator to manage the robot.
	*
	*/
	RobotOperator* RobotOp;
	/*! \brief This is a LaserSensor class object
	*
	*	"laserSen" allows the user to reach to the Laser sensor values.
	*
	*/
	LaserSensor* laserSen;
	
	PioneerRobotInterface* RobotInt;
	/*! \brief This is a RobotControl class object
	*
	*	"rc" allows the user to manage the robot.
	*
	*/
	RobotControl* rc;
	/*! \brief This is a LaserSensor class object
	*
	*	"ls" informs user about the laser sensor of the robot.
	*
	*/
	LaserSensor* ls;
	/*! \brief This is an integer variable
	*
	*	"option" keeps the selection of user.
	*
	*/
	int option, index = 0;
	
public:
	/*! \brief This is a public integer variable
	*
	*	"flag" checks if the robot is connected.
	*
	*/
	int flag;
	/*! \brief This is a constructor.
	* It sets rc, ls,ss,RobotOp, RobotInt ,option and flag to zero as initial values.
	*/
	Menu();
	/*! \brief This is a Destructor.
	* It deletes rc, ls,ss,RobotOp, RobotInt pointers to save memory.
	*/
	~Menu();
	/*! \brief This is a connection function
	*
	*	this function connects or disconnects the robot.
	*
	*/
	void connection();
	/*! \brief This is a motion function
	*
	*	this function helps the robot move and record the path values.
	*
	*/
	void motion();
	/*! \brief This is a sensor function
	*
	*	this function informs user about the laser sensor's range, minimum and maximum
	*	values.
	*
	*/
	void sensor();
	/*! \brief This is a display function
	*
	*	this function is the first step of menu and categorizes the options by content.
	*
	*/
	void display();
	/*! \brief This is an operator creation function
	*
	*	this function allows user to create a new operator or go on with default operator.
	*
	*/
	void Set_Operator();
};

