#include "Encryption.h"
#include <math.h>
using namespace std;
/**
 * @file Encryption.cpp
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This class includes functions that encrypting and decrypting the incoming codes and writing them in a format
 *
 */
int Encryption:: encrypt(int code) {
	int closeCode,i=3,j,num[4],temp;
	if (code > 9999) {
		cout << "You cannot enter more than 4 digits of code!" << endl; 
		return -1;
	}
	if (code < 1000) {
		cout << "You cannot enter less than 4 digits of code!" << endl; 
		return -1;
	}
	temp = code;
	/**
	*Separates incoming numbers into their digits and add 7 to each
	*then I got the mod with 10
	*/
	while (temp != 0) {
		j = temp % 10;
		j = j + 7;
		j = j % 10;
		num[i] = j;
		i--;
		temp = temp / 10;
	}
	/**
	* Swapped the 1st and 3rd and 2nd and 4th digits
	*/
	j= num[2];
	num[2] = num[0];
	num[0] = j;
	j = num[3];
	num[3] = num[1];
	num[1] = j;
	temp = num[0] * 1000 + num[1] * 100 + num[2] * 10 + num[3];
	return temp;
}
int Encryption::decrypt(int code) {
	if (code > 9999) {
		cout << "You cannot enter more than 4 digits of code!" << endl;
		return -1;
	}
	if (code < 1000) {
		cout << "You cannot enter less than 4 digits of code!" << endl;
		return -1;
	}
	int num[4], i=3, j, temp;
	temp = code;
	/**
	*Separates incoming encrypted code into their digits 
	* then swapped the 1st and 3rd and 2nd and 4th digits
	*/
	while (temp != 0) {
		j = temp % 10;
		num[i] = j;
		i--;
		temp = temp / 10;
	}
	if (code <= 999)
		num[0] = 0;
	j = num[0];
	num[0] = num[2];
	num[2] = j;
	j = num[1];
	num[1] = num[3];
	num[3] = j;
	i = 0;
	/// 3 is added to each digit and the mode is taken with 10
	while (i != 4) {
		num[i]=(num[i] + 3)%10;
		i++;
	}
	temp = num[0] * 1000 + num[1] * 100 + num[2] * 10 + num[3];
	return temp;
}
void Encryption::writecode(int code) {
	if (code < 1000) 
		cout << "0";
	if (code < 100)
		cout << "0";
	cout << code;
}