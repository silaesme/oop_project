/**
* @file SonarSensor.h
* @Author Zeynep G�rl�zer (152120191050@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief This file includes SonarSensor class's definition.
*/
#pragma once
#include "PioneerRobotAPI.h"
#include "RangeSensor.h"
class SonarSensor : public RangeSensor
{
/*! \class SonarSensor SonarSensor.h
 *  \brief This is a SonarSensor class
 *
 *  This class provides distance data and management for sonar distance sensor.
 *
 */
private:
	/*! \brief This is a float array.
	*
	*	"ranges" keeps current sonar sensor distance values of the robot.
	*
	*/
	float ranges[16];

	/*! \brief This is a float array.
	*
	*	"angle" keeps angles of sensor.
	*
	*/
	float angle[16] = { 90,50,30,10,-10,-30,-50,-90,-90,-50,-30,-10,10,30,50,90 };

	PioneerRobotAPI* robotAPI;
public:
	SonarSensor();
	~SonarSensor();

	/*! \brief This is a function gets the range which given index.
	*
	*	@param index
	*   \return ranges[index]
	*
	*/
	float getRange(int index);

	/*! \brief This is a function gets max range.
	*
	*	@param &index
	*   \return max
	*
	*/
	float getMax(int& index);

	/*! \brief This is a function gets min range.
	*
	*	@param &index
	*   \return min
	*
	*/
	float getMin(int& index);

	/*! \brief This is a function copies the datas from "PioneerRobotAPI.h" to ranges array.
	*
	*	@param ranges[]
	*   \return nothing
	*
	*/
	void updateSensor(float ranges[]);

	/*! \brief This is an operator function gets the range which given index.
	* 
	*   This function is similar to getRange function.
	*	@param i
	*   \return nothing
	*
	*/
	float operator[](int i);

	/*! \brief This is a function gets the angle which given index.
	*
	*   The angles are manual.
	*	@param index
	*   \return ranges[index]
	*
	*/
	float getAngle(int index);

	/*! \brief This is a function returns minimum distance between two angle index. Also, &angle is the angle which has minimum distance.
	*
	*   The angles are manual.
	*	@param startAngle, endAngle, &angle
	*   \return min_distance
	*
	*/
	float getClosestRange(float startAngle, float endAngle, float& angle);
};

