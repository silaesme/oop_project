#include "Path.h"
#include"Pose.h"
#include <iostream>

void Path::addPos(Pose pose)
{
	Node* node = new Node(pose);
	// If adding first node
	if (head == NULL)
	{
		head = node;
		tail = node;
	}
	// Add to the end of the list
	else 
	{
		Node* p = tail;
		p->next = node;
		tail = node;
	}
	number++;
}
void Path::removePos(int index)
{
	if (index <= number - 1 && index >= 0)
	{
		Node* p = head;
		Node* q = NULL;
		Node* z = NULL;
		// Walk on the list
		for (int i = 0; i < index ; i++)
		{
			if (i == index - 1) z = q; // Need q->prev when remove last element
			q = p;
			p = p->next;
		}
		// Deleting first node of the list
		if (q == NULL)
		{
			head = p->next;
			delete p;
		}
		// Deleting last node of the list
		else if (p->next == NULL)
		{
			q->next = NULL;
			tail = q;
			delete p;
		}
		else
		{
			q->next = p -> next;
			delete p;
		}
		number--;
	} //end-if
	else
	{
		std::cerr << "Invalid index!" << std::endl;
	}
}
void Path::insertPos(int index, Pose pose)
{

	if (index <= number - 1 && index >= 0 )
	{
		Node* node = new Node(pose);
		// Insert first node
		if (head == NULL) { head = node; tail = node; number++;}
		// Add end of the list
		else if (number == index + 1) addPos(pose);
		else
		{
			Node* p = head;
			Node* q = NULL;
			// Walking on list
			for (int i = 0; i < index + 1; i++)
			{
				q = p;
				p = p->next;
			}
			// Insert between two node
			q->next = node;
			node->next = p;
			number++;
		}
		// Increase number of elements
		
	} //end-if
	else
	{
		std::cerr << "Invalid index!" << std::endl;
	}

}
Pose Path::getPos(int i)const
{
	if (i >= 0 && i <= number - 1)
	{
		int index = 0;
		Node* node = this->head;
		while (index != i)
		{
			node = node->next;
			index++;
		}
		return node->pos;
	}
}
void Path::print() const
{
	Node* node = head;
	for (int i = 0; i < number && node; i++)
	{
		Pose p = node->pos;
		std::cout << "[" << i  << "] ---> ";
		std::cout << p << "\n";
		node = node->next;
	}
}
Pose Path::operator[](int i)const
{
	return getPos(i);
}
