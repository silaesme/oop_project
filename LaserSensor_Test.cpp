#include "PioneerRobotAPI.h"
#include "LaserSensor.h"
//#include "Aria/Aria.h"
#include <iostream>
using namespace std;
int main() {
	LaserSensor* sensor = new LaserSensor;
	PioneerRobotAPI* robot = new PioneerRobotAPI;
	float ranges[1000];

	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}
	robot->moveRobot(100);
	cout << endl << "LaserSensor::updateSensor Test" << endl;
	robot->getLaserRange(ranges);
	sensor->updateSensor(ranges);
	ranges == NULL ? cout << "LaserSensor::updateSensor Test is failed!" : cout << "LaserSensor::updateSensor Test is successful!" << endl;


	cout << endl << "LaserSensor::getRange Test" << endl;
	cout << "Ranges of Laser Sensor are [ ";
	for (int i = 0; i < 181; i++) {
		cout << sensor->getRange(i) << " ";
	}
	cout << "]" << endl;

	cout << endl << "LaserSensor::operator[] Test" << endl;
	cout << "Ranges of Laser Sensor are [ ";
	for (int i = 0; i < 181; i++) {
		cout << sensor->operator[](i) << " ";
	}
	cout << "]" << endl;

	int index1 = 0;
	cout << endl << "LaserSensor::getMin Test" << endl;
	cout << "Minimum range is " << sensor->getMin(index1) << " [index: " << index1 << "]" << endl;

	int index2 = 0;
	cout << endl << "LaserSensor::getMax Test" << endl;
	cout << "Maximum range is " << sensor->getMax(index2) << " [index: " << index2 << "]" << endl;

	cout << endl << "LaserSensor::getAngle Test" << endl;
	cout << "Angles of Laser Sensor are [ ";
	for (int i = 0; i < 181; i++) {
		cout << sensor->getAngle(i) << " ";
	}
	cout << "]" << endl;

	float angle = 0;
	cout << endl << "LaserSensor::getClosestRange Test" << endl;
	cout << "The closest distance between angles 5 and 10 is [" << sensor->getClosestRange(5, 10, angle) << "]" << endl;
	cout << "The Angle is [" << angle << "]";

	cout << endl << endl;
	system("pause");
}
