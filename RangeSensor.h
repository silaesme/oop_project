/**
* @file RangeSensor.h
* @Author Zeynep G�rl�zer (152120191050@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief This file includes RangeSensor class's definition.
*/
#pragma once
#include"PioneerRobotAPI.h"
class RangeSensor
{
	/*! \class RangeSensor RangeSensor.h
 *  \brief This is a RangeSensor abstract class
 *
 *  This class is an interface for LaserSensor and SonarSensor.
 *
 */
private:
	float ranges[181];
	PioneerRobotAPI* robotAPI;
public:
	RangeSensor() { robotAPI = new PioneerRobotAPI; }
	~RangeSensor() { delete robotAPI; }

	virtual float getRange(int index) = 0;
	virtual float getMax(int& index) = 0;
	virtual float getMin(int& index) = 0;
	virtual void updateSensor(float ranges[]) = 0;
	virtual float operator[](int i) = 0;
	virtual float getAngle(int index) = 0;
	virtual float getClosestRange(float startAngle, float endAngle, float& angle) = 0;
};

