#include<fstream>
#include<iostream>
#include<string>
#include"Record.h"
using namespace std;

int main()
{

	Record R;

	R.setFileName("fileName.txt");

	if (R.openFile() != true)
		return 0;

	R.writeLine("new\n");
	R.writeLine("line\n");
	R.writeLine("added\n");

	if (R.closeFile() != true)
		return 0;

	if (R.OpenExistingFile() != true)
		return 0;

	string str;
	str = R.readLine();

	if (str != "new")
		return 0;

	str = R.readLine();

	if (str != "line")
		return 0;

	if (R.closeFile() != true)
		return 0;

	return 10;
}

