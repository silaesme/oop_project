#include "Pose.h"
#include "PioneerRobotAPI.h"
#include <math.h>
#define RADYAN 57.2957795131
Pose::Pose()
{
	x = 0;
	y = 0;
	th = 0;
}

Pose::Pose(float _x, float _y, float _th)
{
	setPose(_x, _y, _th);
}

Pose::~Pose(){}

void Pose::setX(float _x)
{
	x = _x;
}

void Pose::setTh(float _th)
{
	th = _th;
}

void Pose::setY(float _y)
{
	y = _y;
}

float Pose::getX()
{
	return x;
}

float Pose::getTh()
{
	return th;
}

float Pose::getY()
{
	return y;
}

bool Pose::operator==(const Pose& other)
{
	if (x == other.x && y == other.y && th == other.th) return true;
	return false;
}

Pose Pose::operator+(const Pose& other)
{
	// th is an angle so it can't be more than 360 degree.
	float tempTh = th + other.th;
	if (tempTh >= 360) tempTh -= 360;
	Pose* pos = new Pose(x + other.x, y + other.y, tempTh);
	return *pos;
}

Pose Pose::operator-(const Pose& other)
{
	// th is an angle so it can't be more than 360 degree.
	float tempTh = th - other.th;
	if (tempTh < 0) tempTh += 360;
	Pose* pos = new Pose(x - other.x, y - other.y, tempTh);
	return Pose();
}

Pose& Pose::operator+=(const Pose& other)
{
	x += other.x;
	y += other.y;
	th += other.th;
	if (th >= 360) th -= 360;
	return *this;
}

Pose& Pose::operator-=(const Pose& other)
{
	x -= other.x;
	y -= other.y;
	th -= other.th;
	if (th < 0) th += 360;
	return *this;
}

bool Pose::operator<(const Pose& other)
{
	if (x < other.x || y < other.y) return true;
	return false;
}

void Pose::getPose(float* _x, float* _y, float* _th)
{
	*_x = x;
	*_y = y;
	*_th = th;
}

void Pose::setPose(float _x, float _y, float _th)
{
	setX(_x); setY(_y); setTh(_th);
}

float Pose::findDistanceTo(Pose pos)
{
	float distance = sqrt(pow(y - pos.y, 2) + pow(x - pos.x, 2));
	return distance;
}

float Pose::findAngleTo(Pose pos)
{
	// dot product between[x1, y1] and [x2, y2]
	float dot = x * pos.x + y * pos.y;
	// determinant
	float det = x * pos.y - y * pos.x;
	return atan2(det, dot) * RADYAN;
}
std::ostream& operator<<(std::ostream& fof, Pose& pose)
{
	fof << "X = " << pose.getX() << " Y = " << pose.getY() << " Angle = " << pose.getTh() << std::endl;
	return fof;
}
std::istream& operator>>(std::istream& fin, Pose& pose)
{
	std::cout << "Enter x, y, th values: ";
	fin >> pose.x >> pose.y >> pose.th;
	return fin;
}