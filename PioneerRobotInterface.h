/**
* @file PioneerRobotInerface.h
* @Author Sila ESME (152120181004@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief This file includes Pose PioneerRobotInerface's definiton.
*/
#pragma once
#include "PioneerRobotAPI.h"
#include "RangeSensor.h"
#include "RobotInterface.h"
#include "LaserSensor.h"
class PioneerRobotInterface : public RobotInterface
{
private:
	PioneerRobotAPI* robotAPI; /**< This member is for access to RobotAPI's commands. */
	RangeSensor* laser; /**< This member is for access to laser operations. */

public:
/** Parametered constructor.
 * This constructor takes parameter and sets it.
 * @param RangeSensor*  laser type
*/
	PioneerRobotInterface(RangeSensor*);
	/** When this function is used, robot turns left.
	*/
	void turnLeft();
	/** When this function is used, robot turns right.
	*/
	void turnRight();
	/** When this function is used, robot goes forward.
	*/
	void forward(float speed);
	/** When this function is used, robot goes backward.
	*/
	void backward(float speed);
	/** This function gets robot's position and sets to pose.
	 * @param Pose&
	 * @return Pose 
	*/
	Pose getPose(Pose&);
	/** This function sets pose.
	 * @param Pose
	*/
	void setPose(Pose);
	/** This function stops robot's move.
	*/
	void stopTurn();
	/** This function same with "stopTurn()", stops robot's move.
	 * Writer comment: I don't know why we write this function or "stopTurn()".
	*/
	void stopMove();
	/** This function updates laser, sonar etc. sensors;
	 * takes robot's initial position.
	*/
	void updateSensors();
	/** This function gets robot's laser sensor values.
	 * @param float& is array that keeps laser sensor's
	 * max and min values. This array returns olny.
	*/
	void getLaser(float&);
	/** This function gets robot's sonar sensor values.
	 * @param float& is array that keeps sonar sensor's
	 * values. This array returns olny.
	*/
	void getSonar(float&);
};

