﻿#include "RobotOperator.h"
#include <iostream>
using namespace std;
/**
 * @file RobotOperatorTest.cpp
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This cpp to use test RobotOperator class
 *
 */
int main(){
/*!
 \param ArrayOperator a RobotOperator array object. 
*/
	int number, state, opcode,index=0;
	string name, surname;
	RobotOperator* Arrayoperator[2];
	///to assign values ​​to the first index of the array operator
	Arrayoperator[0]=new RobotOperator("Merve", "Kati", 1, 1234);
	Arrayoperator[0]->print();
	if (Arrayoperator[0]->checkAccessState())
		cout << "The operator has access." << endl;
	///gets operator information from the user
	cout << "Enter operator name and surname : ";
	cin >> name;
	cin >>surname;
	cout << "Enter the operator access state : ";
	cin >> state;
	cout << "Enter the operator access code : ";
	cin >> opcode;
	///sets operator information received from the user
	Arrayoperator[1] = new RobotOperator(name,surname,state,opcode);
	Arrayoperator[1]->print();

	cout << "Enter a number to encrypted the number : ";
	cin >> number;
	if (number >= 1000) {
		opcode=Arrayoperator[0]->encryptCode(number);
		cout << "The encrypted code : " << opcode << endl;
	}
	else
		cout << "you can not enter a number less than 4 digits !"<<endl;
	///finds the operator according to the access code entered by the user
	cout << "Enter a access code to reach operator : ";
	cin >> opcode;
	if (opcode >= 1000) {
		for (int i = 0; i < 2; i++) {
			if (Arrayoperator[i]->checkAccessCode(opcode)) {
				Arrayoperator[i]->print();
				index++;
			}
			
		}
		if (index == 0) {
			cout << "No operator have this access code found! " << endl;
		}
	}
	else
		cout << "you can not enter a number less than 4 digits !"<<endl;


	system("pause");
} 