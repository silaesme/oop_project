#include<fstream>
#include<iostream>
#include<string>
#include"Record.h"

using namespace std;

bool Record::openFile()
{
	file.open(fileName, ios::out);

	if (file.is_open() == true)
		return true;
	else
		return false;
}
bool Record::OpenExistingFile()
{
	file.open(fileName);
	if (file.is_open() == true)
		return true;
	else
		return false;
}
bool Record::closeFile()
{
	file.close();
	if (file.is_open() == true)
		return false;
	else
		return true;
}
void Record::setFileName(string name)
{
	fileName = name;
}
string Record::readLine()
{
	string line;

	file >> line;
	return line;
}
bool Record::writeLine(string str)
{
	file << str;
	cout << endl;
	file << endl;
	return true;
}

