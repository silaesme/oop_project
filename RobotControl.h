#pragma once
#include "PioneerRobotAPI.h"
#include "Pose.h"
#include <iostream>
#include "RobotOperator.h"
#include "RobotInterface.h"
#include "RangeSensor.h"
/**
 * @file RobotControl.h
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This class includes functions that enable the robot to move and direct.
 *
 * This class provide the robot move on, to go back and turn through composition relationship between
 * classes RobotControl,Pose and PioneerRobotAPI.
 */
class Path;
class RobotInterface;
class RobotControl
{
private:
	Pose* position; /*!<To obtain and set position informations */
	Path* path;
	bool accesControl; /*!<To check acces code */
	RobotOperator* Roperator; /*!<To create an operator */
	RobotInterface* robot;
	RangeSensor* laser;
public:
	RobotControl(RobotInterface* robotI, RangeSensor* range, RobotOperator* rope); //! RobotControl constructor. 
	~RobotControl(); //! RobotControl destructor.
	/** Used to connect to the robot. Ths function should be called before others.
	* @return true if the connection is success, otherwise false
	*/
	bool connect();
	/** Used to disconnect to the robot. Ths function should be called before quiting from program.
	* @return true if the connection is success, otherwise false
	*/
	bool disconnect();
	/** Rotates the robot.
	* @param dir required direction to turn the robot left
	*/
	void turnLeft();
	/** Rotates the robot.
	* @param dir required direction to turn the robot right.
	*/
	void turnRight();
	/** moves forward the robot at the given speed.
	* @param speed required motion speed for the robot (millimeter/seconds)
	*/
	void forward(float);
	/** print the robot information
	* @param print position,ranges and sonars
	*/
	void print();
	/** moves backward the robot at the given speed.
	* @param speed required motion speed for the robot (millimeter/seconds)
	*/
	void backward(float);
	/** gets position information
	* @param rhis function type Pose
	*/
	Pose getPose();
	/** Sets the positon of the robot.
	* @param x the required x position of the robot (the values are in millimeters)
	* @param y the required y position of the robot (the values are in millimeters)
	* @param th the required orientation angle of the robot (the values are in degrees)
	*/
	void setPose(Pose&);
	/** To make the robot stops turning while turning
	*/
	void stopTurn();
	/** To stop robot
	*/
	void stopMove();
	/** Used to this function adds the position of the robot to path.
	* @return true if the Acces control is true.
	*/
	bool addToPath();
	/** Used to Deletes positions in path.
	* @return true if the Acces control is true.
	*/
	bool clearPath();
	/** Used to save to path in path.txt file
	* @return true if the Acces control is true.
	*/
	bool recordPathToFile();
	/** Used to make Access control is true or false.This fuction check operator code.
	* @return true if the code is true.
	*/
	bool openAccess(int);
	/** Used to make access control true or false and check operator code.
	* �f code is true make access control is false and close access.
	* @return true if the connection is success, otherwise false
	*/
	bool closeAccess(int);
};