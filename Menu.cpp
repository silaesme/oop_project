#include "Menu.h"
#include<iostream>
#include<stdlib.h>
#include<string>
#include"Pose.h"
#include "Aria/Aria.h"

using namespace std;
Menu::Menu()
{
	ss = new SonarSensor();
	ls = new LaserSensor();
	RobotInt = new PioneerRobotInterface(laserSen);	
	option = 0;
	flag = 0;
}
Menu::~Menu()
{
	delete RobotInt;
	delete RobotOp;
	delete rc;
	delete ls;
	delete ss;
}
void Menu::Set_Operator()
{
	string name="", surname="";
	int option, code;
	cout << "<CREATE OPERATOR>" << endl;
	cout << endl << "1.CREATE AN OPERATOR" << endl << "2.USE DEFAULT OPERATOR" << endl << endl << "CHOOSE ONE :";
	cin >> option;
	cout << endl;
	switch (option)
	{
	case 1:
		system("CLS");
		cout << "<CREATE AN OPERATOR>" << endl;
		cout << "enter name:";
		cin >> name;
		cout << endl << "enter surname:";
		cin >> surname;
		cout << endl << "enter a uniqe access code (4 digits) :";
		cin >> code;
		cout << endl;
		RobotOp = new RobotOperator(name,surname,1,code);
		rc = new RobotControl(RobotInt, ls, RobotOp);
		display();
		break;
	case 2:
		system("CLS");
		RobotOp = new RobotOperator();
		rc = new RobotControl(RobotInt, ls, RobotOp);
		display();
		break;
	}
}
void Menu::connection()
{
	cout << "CONNECTION MENU" << endl << endl;
	cout << "1.CONNECT ROBOT" << endl << "2.DISCONNECT ROBOT" << endl << "3.BACK" << endl;
	cout << endl<<"CHOOSE ONE: ";
	cin >> option;
	switch (option)
	{
	case 1:
		system("CLS");
		cout << "<CONNECT>" << endl << "Robot is connected...";
		rc->connect();											
		flag = 1;
		connection();
		break;
	case 2:
		system("CLS");
		cout << "<DISCONNECT>" << endl << "Robot is disconnected...";
		rc->disconnect();
		connection();
		break;
	case 3:
		system("CLS");
		display();
		break;
	default:
		system("CLS");
		cout << "INVALID OPTION... PLEASE SELECT AGAIN" << endl;
		connection();
		break;
	}
}
void Menu::motion()
{
	cout << "MOTION MENU" << endl << endl;
	cout << "1.MOVE ROBOT" << endl << "2.TURN LEFT" << endl << "3.TURN RIGHT" << endl
		<< "4.FORWARD" << endl << "5.MOVE DISTANCE" << endl << "6.STOP MOVE" << endl << "7.ADD TO PATH" << endl
		<< "8.CLEAR PATH" << endl << "9.RECORD PATH TO FILE" << endl << "10.OPEN ACCESS" << endl << "11.CLOSE ACCESS" << endl
		<< "12.PRINT POSITION AND RANGES" << endl << "13.BACK" << endl;
	cout << endl << "CHOOSE ONE: ";
	cin >> option;
	switch (option)
	{
	case 1:
		system("CLS");
		cout << "<MOVE ROBOT>" << endl;
		cout << "1.FORWARD" << endl << "2.BACKWARD" << endl;
		cout << endl << "CHOOSE ONE: ";
		cin >> option;
		switch (option)
		{
		case 1:
			system("CLS");
			cout << "moving forward..." << endl;
			rc->forward(500);
			motion();
			break;
		case 2:
			system("CLS");
			cout << "moving backward..." << endl;
			rc->backward(500);
			motion();
			break;
		}
		break;
	case 2:
		system("CLS");
		cout << "turning left..." << endl;
		rc->turnLeft();
		motion();
		break;
		
	case 3:
		system("CLS");
		cout << "turning right..." << endl;
		rc->turnRight();
		motion();
		break;
	case 4:
		system("CLS");
		cout << "moving forward..." << endl;
		rc->forward(500);
		motion();
		break;
	case 5:
		system("CLS");
		cout << "moving backward..." << endl;
		rc->backward(500);
		motion();
		break;
	case 6:
		system("CLS");
		cout << "stopping..." << endl;
		rc->stopMove();
		motion();
		break;
	case 7:
		system("CLS");
		cout << "adding to path..." << endl;
		rc->addToPath();
		motion();
		break;
	case 8:
		system("CLS");
		cout << "path is cleaned..." << endl;
		rc->clearPath();
		motion();
		break;
	case 9:
		system("CLS");
		cout << "path is recording to file..." << endl;
		rc->recordPathToFile();
		motion();
		break;
	case 10:
		system("CLS");
		int code;
		cout << "please enter the access code : " << endl;
		cin >> code;
		cout << "Robot acces is opening ..." << endl;
		rc->openAccess(code);
		motion();
		break;
	case 11:
		system("CLS");
		int Acode;
		cout << "please enter the access code : " << endl;
		cin >> Acode;
		cout << "Robot acces is closing..." << endl;
		rc->closeAccess(Acode);
		motion();
		break;
	case 12:
		system("CLS");
		rc->print();
		motion();
		break;
	case 13:
		system("CLS");
		display();
		break;
	default:
		system("CLS");
		cout << "INVALID OPTION... PLEASE SELECT AGAIN" << endl;
		motion();
		break;
	}
}
void Menu::sensor()
{
	cout << "SENSOR MENU" << endl << endl;	

	cout << "1.GET RANGE" << endl << "2.GET MAXIMUM RANGE" << endl << "3.GET MINIMUM RANGE" << endl
		<< "4.GET ANGLE" << endl << "5.GET CLOSEST RANGE" << endl << "6.BACK" << endl;
	cout << endl << "CHOOSE ONE: ";
	cin >> option;
	switch (option)
	{
	case 1:
		system("CLS");
		cout << "<GET RANGE>" << endl << "get sonar range:" << endl;
		for (int j = 1; j < 16; j++)
		{
			cout << ss->getRange(j) << " ";
		}
		cout << endl << "get laser range" << endl;
		for (int i = 1; i < 181; i++)
		{
			cout << ls->getRange(i) << " ";
		}
		cout << endl;
		sensor();
		break;
	case 2:
		system("CLS");
		cout << "<GET MAXIMUM RANGE>" << endl;
		cout << "<GET RANGE>" << endl << "get maximum range of sonar sensor: " << endl;
		for (int i = 1; i < 16; i++)
		{
			cout << ss->getMax(i) << " ";
		}
		cout << endl << "get maximum range of laser sensor: " << endl;
		for (int j = 1; j < 181; j++)
		{
			cout << ls->getMax(j) << " ";
		}
		cout << endl;
		sensor();
		break;

	case 3:
		system("CLS");
		cout << "<GET MINIMUM RANGE>" << endl;
		cout << "<GET RANGE>" << endl << "get minimum range of sonar sensor: " << endl;
		for (int i = 1; i < 16; i++)
		{
			cout << ss->getMin(i) << " ";
		}
		cout << endl << "get minimum range of laser sensor: " << endl;
		for (int j = 1; j < 181; j++)
		{
			cout << ls->getMin(j) << " ";
		}
		sensor();
		break;
	case 4:
		system("CLS");
		cout << "<GET ANGLE>" << endl;
		cout << "<GET RANGE>" << endl << "get angle of sonar sensor: " << endl;
		for (int i = 1; i < 16; i++)
		{
			cout << ss->getAngle(i) << " ";
		}
		cout << endl << "get angle of laser sensor: " << endl;
		for (int j = 1; j < 181; j++)
		{
			cout << ls->getAngle(j) << " ";
		}
		cout << endl;
		sensor();
		break;
	case 5:
		system("CLS");
		cout << "<GET CLOSEST RANGE>" << endl;
		//cout<<ls->getClosestRange(position->getX(), position->getY(),) << endl;
		sensor();
		break;
	case 6:
		system("CLS");
		display();
		break;
	default:
		system("CLS");
		cout << "INVALID OPTION... PLEASE SELECT AGAIN" << endl;
		sensor();
		break;
	}
	
}
void Menu::display()
{
	std::cout << std::endl << "!!!!!!please select an operating option first(4.CREATE OPERATOR) then connect the robot!!!!!! " << std::endl << std::endl;

	cout << "MAIN MENU" << endl << endl;
	cout << "1.CONNECTION" <<endl << "2.MOTION" <<endl << "3.SENSOR" <<endl << "4.CREATE OPERATOR" << endl<<"5.QUIT"<<endl;
	cout <<endl<< "CHOOSE ONE: ";
	cin >> option;
	switch (option)
	{
	case 1:
		system("CLS");
		connection();
		break;
	case 2:
		system("CLS");
		motion();
		
		break;
	case 3:
		system("CLS");
		sensor();
		break;
	case 4:
		system("CLS");
		Set_Operator();
		break;
	case 5:
		system("CLS");
		exit(1);
		break;
	default:
		system("CLS");
		cout << "INVALID OPTION... PLEASE SELECT AGAIN" << endl;
		display();
		break;
	}
}