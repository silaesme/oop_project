/**
* @file Node.h
* @Author Sıla EŞME (152120181004@ogrenci.ogu.edu.tr)
* @date January, 2021
* @brief This file includes Node class's (struct) definiton and implementation.
*/
#ifndef _NODE_H_
#define _NODE_H_
#include "Pose.h"
/** This struct is Node which includes data and next struct's address.
*/
struct Node
{
	Pose pos;
	Node* next;

	Node(Pose _pos) { pos = _pos; next = NULL; }
	Node() {}

};
#endif // !_NODE_H_
