#include "LaserSensor.h"
#include "PioneerRobotAPI.h"
LaserSensor::LaserSensor() {
	robotAPI = new PioneerRobotAPI;
	robotAPI->getLaserRange(ranges);
}

LaserSensor::~LaserSensor() {
	delete robotAPI;
}

float LaserSensor::getRange(int index) { return ranges[index]; }

float LaserSensor::getMax(int& index) {
	float max = ranges[0];
	index = 0;
	for (int i = 1; i < 181; i++) {
		if (ranges[i] > max) {
			max = ranges[i];
			index = i;
		}
	}
	return max;
}

float LaserSensor::getMin(int& index) {
	float min = ranges[0];
	index = 0;
	for (int i = 1; i < 181; i++) {
		if (ranges[i] < min) {
			min = ranges[i];
			index = i;
		}
	}
	return min;
}

void LaserSensor::updateSensor(float ranges[]) {
	for (int i = 0; i < 181; i++)
		this->ranges[i] = ranges[i];
}

float LaserSensor::operator[](int i) { return ranges[i]; }

float LaserSensor::getAngle(int index) { return (float)index; }

float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle) {
	float min_distance = ranges[(int)startAngle];
	angle = startAngle;
	for (int i = (int)(startAngle + 1); i <= int(endAngle); i++) {
		if (min_distance > ranges[i]) {
			min_distance = ranges[i];
			angle = i;
		}
	}
	return min_distance;
}
