#pragma once
#include<string>
#include "Encryption.h"
using namespace std;
/**
 * @file RobotOperator.h
 * @Author Merve Kati 152120171025 (mervekati350@gmail.com)
 * @date January, 2021
 * @brief This class includes functions that gets Operator information 
 *
 *This class provide the robot operator through composition relationship between
 * classes Encryption and RobotOperator
 */
class RobotOperator
{
private:
	string name; /*!< The operator name */
	string surname; /*!< The operator surname*/
	const unsigned int accessCode; /*!< The operator encrypted code */
	bool accessState; /*!< The operator access state  */
	unsigned int controlcode;
	Encryption enc; /*!<To compositon and to get encrypted code */
public:
	//! RobotOperator constructor.
	RobotOperator();
	RobotOperator(string, string, bool,int);
	//! RobotOperator destructor.
	~RobotOperator();
	/** Used to get Operator name
	* @return name of operator
	*/
	string getName() ;
	/** Used to get Operator name
	* @return surname of operator
	*/
	string getSurName();
	/** Used to get Operator access code
	* @return access code of operator
	*/
	int getAccessCode();
	/** Used to set Operator name
	*/
	void setName(string);
	/** Used to set Operator surname
	*/
	void setSurName(string);
	/** Used to set Operator access state
	*/
	void setaccessState(bool);
	/** Used to encrypt the incoming code
	*@return encrypted code
	*/
	int encryptCode(int);
	/** Used to decrypt the incoming code
	*@return decrypted code
	*/
	int decryptCode(int);
	/** Used to control the incoming code
	*@return true, if incoming code equal to access code
	*/
	bool checkAccessCode(int);
	/** Used to control the state
	*@return true, if state is equal to 1
	*/
	bool checkAccessState();
	/** 
	* Used to print operator information
	*/
	void print();
};

